from re import search
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views


# Create your tests here.

def test_url_prof_exist(self):
    response = Client().get('/webservice_sim')
    self.assertEqual(response.status_code, 200)

def test_url_validator_email_exist(self):
    response = Client().get('/check_email')
    self.assertEqual(response.status_code, 200)

def test_url_sub_json_exist(self):
    response = Client().get('/sub_json')
    self.assertEqual(response.status_code, 200)

def test_url_unsub_exist(self):
    response = Client().get('/unsubscribe')
    self.assertEqual(response.status_code, 200)

def test_url_val_exist(self):
    response = Client().get('/subscribe')
    self.assertEqual(response.status_code, 301)

def test_using_prof_func(self):
    found = resolve('/webservice_sim')
    self.assertEqual(found.func, views.webservice_sim)

def test_using_validator_func(self):
    found = resolve('/check_email')
    self.assertEqual(found.func, views.check_email)

def test_using_sub_json(self):
    found = resolve('/sub_json')
    self.assertEqual(found.func, views.sub_json)

def test_using_sub_json(self):
    found = resolve('/unsubscribe')
    self.assertEqual(found.func, views.sub_json)

def test_using_subscribe(self):
    found = resolve('/subscribe')
    self.assertEqual(found.func, views.sub)