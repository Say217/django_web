var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#email-form").keyup(function() {
        checkEmail();
    });

    $("#pass-form").keyup(function() {
        $('#passwordStatus').html('');
        if ($('#pass-form').val().length < 8) {
            $('#passwordStatus').append('<small style="color: gray"> must be at least 8 characters</small>');
        }
        checkAll();
    });


    $('#subs-button').click(function () {	
        data = {
            'email' : $('#email-form').val(),
            'name' : $('#name-form').val(),
            'password' : $('#pass-form').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : '/subscribe/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('email-form').value = '';
                document.getElementById('name-form').value = '';
                document.getElementById('pass-form').value = '';
                $('#statusForm').html('');
                $('#subs-button').prop('disabled', true);
                renderTable();
            }
        });
    });

    $(document).on('click', '.unsub', function(){
        var id = $(this).attr('id');
        $.ajax({
            method : "POST",
            url : "/unsubscribe/",
            data : {'email': id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
            dataType : "json",
        })
        renderTable();  //bug pada unsubscribe. renderTable jalan terlebih dahulu sebelum views selesai memproses data.
    });
});

function renderTable(){
    console.log("tai");
    $.ajax({
        url : "/sub_json/",
        dataType : "json",
        success : function (datajson) {
            var dataSubs = datajson.subscribers;
            $('#sub_table').html('');
            for (var i = 0; i < dataSubs.length; i++) {
                var info = dataSubs[i];
                var name = info.name;
                var email = info.email;
                var email_id = email.replace(/[^\w\s!?]/g,'');
                console.log(email_id);
                var line_table = 
                    '<tr id = "row' + email_id + '">' +
                        "<th scope=\"row\">" + name + 
                        "</th>" +
                        "<td>" + email + 
                        "</td>" +
                        "<td>" +
                            '<button class="button bg-danger border rounded unsub" style="color:white;" id = "' + email + '">' +
                            '<span class="yusuf-bold">Unsubscribe</span>' +
                            '</button>' +
                        "</td>" +
                    "</tr>";
                $('#sub_table').append(line_table);
            };
        },

        error: function (error) {
            var line_table = "<tr><td colspan=\"3\" class = \"text-center\">JSON failed to be loaded :(</td></tr>";
            $('#sub_table').append(line_table);
        },
    });
}

function checkEmail() {
    data = {
        'email':$('#email-form').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: '/check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#emailStatus').html('');
            if (data['status'] == 'failed') {
                emailIsValid = false;
                $('#subs-button').prop('disabled', true);
                $('#emailStatus').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#emailStatus').append('<small style="color:green">' + data["message"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#name-form').val() !== '' && 
        $('#pass-form').val() !== '' &&
        $('#pass-form').val().length > 7) {
        
        $('#subs-button').prop('disabled', false);
        $('#subs-button').css("background", "#0066CC");
        $('#subs-button').css("cursor", "pointer");
    } else {
        $('#subs-button').prop('disabled', true);
        $('#subs-button').css("background", "gray");
        $('#subs-button').css("cursor", "not-allowed");
    }
}