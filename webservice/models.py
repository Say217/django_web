from django.db import models

# Create your models here.
class WSdata(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True, db_index=True)
    password = models.CharField(max_length=100)

    def to_dict(self):
    	return{
    	"name": self.name,
    	"email": self.email,
    	}