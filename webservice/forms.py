from django import forms

class WSform(forms.Form):
	pass_attrib = {
				'type': 'password',
	 			'class' : 'form-control',
	 			'id' : 'pass-form',
	 			'placeholder' : 'Password',
	 			}
	email_attrib = {
				'name':'email',
				'type': 'text', 
				'class' : 'form-control', 
				'id' : 'email-form', 
				'placeholder' : 'email@example.com'
				}
	name_attrib = {
				'type': 'text',
				'class' : 'form-control',
				'id' : 'name-form',
				'placeholder' : 'Your Name'
				}

	name = forms.CharField(max_length = 100, error_messages={"required": "Type your name here"}, widget = forms.TextInput(attrs=name_attrib))
	email = forms.EmailField(error_messages={"required": "Email is not valid"}, max_length=100, label='Email', required=True, widget=forms.EmailInput(attrs=email_attrib))
	password = forms.CharField(label='Password', required=True, max_length = 100, min_length=8, widget = forms.PasswordInput(attrs=pass_attrib), error_messages={"min_length": "8 characters minimum"})
