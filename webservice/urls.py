from django.urls import path
from webservice import views

urlpatterns = [
    path('webservice_sim/', views.webservice_sim, name='webservice_sim'),
    path('check_email/',views.check_email, name='check_email'),
    path('subscribe/',views.subscribe, name="subscribe"),
    path('sub_json/', views.sub_json, name="sub_json"),
    path('unsubscribe/', views.unsubscribe, name="unsubscribe")
]
