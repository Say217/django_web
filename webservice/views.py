from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render,redirect
import urllib.request, json, requests
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from .forms import WSform
from .models import WSdata
import json

# Create your views here.

def webservice_sim(request):
    response = {
        'subscribe_form' : WSform
    }
    context={'subscribe':'active'}
    return render(request, "webservice_sim.html", response)

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Email is not valid',
            'status':'failed'
        })

    exist = WSdata.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'Email already exist',
            'status':'failed'
        })
        
    
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def subscribe(request):
    if (request.method == "POST"):
       	subscriber = WSdata(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            'message':'Subscribed'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"Can't execute your request"
        }, status = 403)

def sub_json(request):
    as_dict = [obj.to_dict() for obj in WSdata.objects.all()]
    return JsonResponse({"subscribers": as_dict}, content_type="application/json")

def unsubscribe(request):
    email = request.POST.get('email', '')
    query = WSdata.objects.filter(email = email)
    if query:
        query.get().delete()
    return redirect ('webservice_sim')