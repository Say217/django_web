var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#email-form").keyup(function() {
        checkEmail();
    });

    $("#pass-form").keyup(function() {
        $('#statusForm').html('');
        if ($('#pass-form').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Harus 8 Huruf cuyyyyyy :D </small>');
        }
        checkAll();
    });


    $('#subs-button').click(function () {	
        data = {
            'email' : $('#email-form').val(),
            'name' : $('#name-form').val(),
            'password' : $('#pass-form').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'save_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('email-form').value = '';
                document.getElementById('name-form').value = '';
                document.getElementById('pass-form').value = '';
                $('#statusForm').html('');
                $('#subs-button').prop('disabled', true);
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#email-form').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: 'email_validator/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'ferguso') {
                emailIsValid = false;
                $('#subs-button').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#name-form').val() !== '' && 
        $('#pass-form').val() !== '' &&
        $('#pass-form').val().length > 7) {
        
        $('#subs-button').prop('disabled', false);
        $('#subs-button').css("background", "blue");
        $('#subs-button').css("cursor", "pointer");
    } else {
        $('#subs-button').prop('disabled', true);
        $('#subs-button').css("background", "red");
        $('#subs-button').css("cursor", "not-allowed");
    }
}